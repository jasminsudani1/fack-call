//
//  DataBaseHelper.swift
//  Fack Call
//
//  Created by Loopbots on 06/01/23.
//

import Foundation
import UIKit
import CoreData

class DataBaseHelper {
    
    static var shareInstance = DataBaseHelper()
        
    func save(name : String , time : Int) {
        
        guard let  appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
        
        let managedContaxt = appDelegate.persistentContainer.viewContext
        
        let userEntity = NSEntityDescription.entity(forEntityName: "Porson", in: managedContaxt)!
        
        let user = NSManagedObject(entity: userEntity, insertInto: managedContaxt)
        
        user.setValue(name , forKey: "name")
        
        user.setValue(time , forKey: "time")
        
        do {
            
            try managedContaxt.save()
            print("data is save")
            
        } catch {
            
            print(error)
            
        }
        
    }

    func fach() {
        
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
        
        let managedContaxt = appDelegate.persistentContainer.viewContext
        
        let fachRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Porson")
        
        do {
            
            let result = try managedContaxt.fetch(fachRequest)
            
            for data in result as! [NSManagedObject] {
                
                print(data.value(forKey: "name") as! String)
                print(data.value(forKey: "time") as! Int)
                
            }
            
        } catch {
            
            print(error)
            
        }
        
    }
    
    
    func delet( name : String) {
        
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
        
        let managedContaxt = appDelegate.persistentContainer.viewContext
        
        let fachRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Student")
        
        fachRequest.predicate = NSPredicate(format: "name = %@", name)
        
        do {
            
            let test = try managedContaxt.fetch(fachRequest)
            
            let objectToDelete = test[0] as! NSManagedObject
            
            managedContaxt.delete(objectToDelete)
            
            do {
                
                try managedContaxt.save()
                
            } catch {
                
                print(error)
                
            }
            
        } catch {
            
            print(error)
            
        }
        
    }
    
    
    func update(name : String, name1 : String, time : Float ) {
        
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
        
        let managedContaxt = appDelegate.persistentContainer.viewContext
        
        let fachRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Student")
        
        fachRequest.predicate = NSPredicate(format: "name = %@", name)
        
        do {
            
            let test = try managedContaxt.fetch(fachRequest)
            
            let objectUpdate = test[0] as! NSManagedObject
            objectUpdate.setValue(name1 , forKey: "name")
            objectUpdate.setValue(time, forKey: "time")
            
            do {
                
                try managedContaxt.save()
                
            } catch {
                
                print(error)
                
            }
            
        } catch {
            
            
            
        }
        
    }
   
}

