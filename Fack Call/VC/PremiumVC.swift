//
//  PremiumVC.swift
//  Fack Call
//
//  Created by Jasmin Sudani on 14/12/22.
//

import UIKit

class PremiumVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var table_Premium: UITableView!
    
    var text_Array = ["Go Premium","Restore Purchase","Rate This App","Share","Feedback"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        table_Premium.delegate = self
        table_Premium.dataSource = self
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return text_Array.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "PremiumCell") as! PremiumCell
        cell.lbl_Text_Name.text = text_Array[indexPath.row]
        cell.view_Premium.layer.cornerRadius = 20
        
        return cell
        
    }

}
