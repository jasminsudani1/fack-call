//
//  TimeVC.swift
//  Fack Call
//
//  Created by Jasmin Sudani on 02/12/22.
//

import UIKit

var timeVc = TimeVC()

class TimeVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
//    let exSetting = ExtraSettingsVC()
    
    let userDefult = UserDefaults.standard
    
    
    @IBOutlet weak var but_Next_To_Caller: UIButton!
    @IBOutlet weak var table_Time: UITableView!
    var arr = [Int]()
    var arr2 = [Int]()
    var number = Int()
    var x = Float()
    
    let lbl_Array = ["5 Seconds Later","10 Seconds Later","30 Seconds Later","1 Minutes Later","2 Minutes Later","5 Minutes Later","10 Minutes Later","20 Minutes Later","30 Minutes Later","1 Hour Later"]
    
    let time_Array = [1,2,3,4,5,6,7,8,9,10]
    
    let time_Array2 : [Float] = [5,10,30,60,120,300,600,1200,1800,3600]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        table_Time.dataSource = self
        table_Time.delegate = self
        but_Next_To_Caller.layer.cornerRadius = but_Next_To_Caller.frame.height / 2
        
    }
 
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return lbl_Array.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "TimeCell") as! TimeCell
        
        cell.lbl_Time_Notification.text = lbl_Array[indexPath.row]
        cell.view_Time.layer.cornerRadius = 20
        
        let z = arr.contains(indexPath.row)
        
        if z == true {
            
            cell.view_Time.layer.borderWidth = 2
            cell.view_Time.layer.borderColor = UIColor(hexString: "5D00CF").cgColor
            cell.img_SelectTicMark.isHidden = false
            
        } else {
            
            cell.view_Time.layer.borderWidth = 0
            cell.img_SelectTicMark.isHidden = true
            
        }
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        userDefult.set(time_Array[indexPath.row], forKey: "myKey")
        
        print(userDefult.float(forKey: "myKey"))
        
        x = userDefult.float(forKey: "myKey")
        
        print(x)
        
        let x = arr.contains(indexPath.row)
        
        if x {
            
            let k = arr.firstIndex(of: indexPath.row)!
            arr.remove(at: k)
            
            let l = arr2.firstIndex(of: indexPath.row)!
            arr2.remove(at: l)
            
        } else {
           
           number = time_Array[indexPath.row]
           arr2.removeAll()
           arr.removeAll()
           arr2.append(indexPath.row)
           arr.append(indexPath.row)
           
       }
        
        tableView.reloadData()
        
    }
    
    @IBAction func but_Time_Back(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
    @IBAction func but_Time_Next(_ sender: UIButton) {

        let x = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CallerVC")
            
        self.navigationController?.pushViewController(x, animated: true)
        
    }
    
}
