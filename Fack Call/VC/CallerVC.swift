//
//  CellerVc.swift
//  Fack Call
//
//  Created by Jasmin Sudani on 03/12/22.
//

import ContactsUI
import Contacts
import UIKit

class CallerVC: UIViewController, UIImagePickerControllerDelegate & UINavigationControllerDelegate, CNContactPickerDelegate {
    
    let fackCall = FackCallVC()
    @IBOutlet weak var textFild_Name: UITextField!
    @IBOutlet weak var but_Caller_Next: UIButton!
    @IBOutlet weak var but_Show_Contct: UIButton!
    @IBOutlet weak var but_Edit_Img: UIButton!
    @IBOutlet weak var but_Select_Img: UIButton!
    @IBOutlet weak var img_Profile: UIImageView!
    let x = UIImagePickerController()
    let exSetting = ExtraSettingsVC()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        img_Profile.layer.cornerRadius = img_Profile.frame.height / 2
        
        but_Caller_Next.layer.cornerRadius = but_Caller_Next.frame.height / 2
        
        let color = UIColor.black.withAlphaComponent(0.50)
        
                textFild_Name.placeholder = "     Name / Phone"
                let placeholder = textFild_Name.placeholder ?? ""
        textFild_Name.attributedPlaceholder = NSAttributedString(string: placeholder, attributes: [NSAttributedString.Key.foregroundColor : color])
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        self.view.endEditing(true)

    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let a = info[.editedImage] ?? info[.originalImage] {
            let a1 = a as? UIImage
        
                img_Profile.image = a1
            
        }
        self.dismiss(animated: true)
    }

    @IBAction func but_Select_img_Action(_ sender: UIButton) {
     
        x.sourceType = .photoLibrary
        x.allowsEditing = true
        x.delegate = self
        self.present(x, animated: true)
        
    }
    
    @IBAction func but_Edit_Action(_ sender: UIButton) {
        
        x.allowsEditing = true
        
        self.present(x, animated: true)
        
    }
    
    @IBAction func but_Caller_Back(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
     
    @IBAction func but_Contact_Import(_ sender: UIButton) {
        
        let vc = CNContactPickerViewController()
        vc.delegate = self
        present(vc, animated: true)
        
    }
    
    @IBAction func but_Caller_Next(_ sender: UIButton) {
       
        fackCall.nameUserDefult.set(textFild_Name.text, forKey: "name")
        
        let x = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "RingtoneVC")
        
        self.navigationController?.pushViewController(x, animated: false)
  
    }
   
    @IBAction func textFild_Name_Number_Action(_ sender: UITextField) {
        
        but_Caller_Next.isHidden = false
        
    }
}
