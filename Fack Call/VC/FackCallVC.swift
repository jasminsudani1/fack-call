//
//  ViewController.swift
//  Fack Call
//
//  Created by Jasmin Sudani on 01/12/22.
//

import UIKit

class FackCallVC : UIViewController, UITableViewDelegate, UITableViewDataSource {
   
    @IBOutlet weak var table_Setting: UITableView!
    
    @IBOutlet weak var view_Alert_BG: UIView!
    
    @IBOutlet weak var table_Profile_Call: UITableView!
    
    let nameUserDefult = UserDefaults.standard
    
    let time = TimeVC()
    
    var x = Int()
    
    var img_Array = [#imageLiteral(resourceName: "Time"), #imageLiteral(resourceName: "User"), #imageLiteral(resourceName: "Bell"), #imageLiteral(resourceName: "Voice"), #imageLiteral(resourceName: "Setting")]
    
    var lbl_Setting_Name = ["Time","Caller","Ringtone","Voice","Extra Settings"]
    
    var lbl_SeteingSelect_Name = ["5 Seconds Later","Super-Boos"]
    
    var lbl_Name_Array = ["Loop Bots","Akshy","PM Modi","Big-Boos","HR Sir"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        
        x = time.userDefult.integer(forKey: "myKey")
        time.userDefult.set(1, forKey: "myKey")
        table_Setting.dataSource = self
        table_Setting.delegate = self
        table_Profile_Call.delegate = self
        table_Profile_Call.dataSource = self
        
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView.tag == 0 {
            
            return img_Array.count
            
        } else {
            
            return lbl_Name_Array.count
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView.tag == 0 {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "SettingCell") as! SettingCell
            
            cell.img_SettingIcone.image = img_Array[indexPath.row]
            
            cell.lbl_Setting_Name.text = lbl_Setting_Name[indexPath.row]
            
            cell.view_SettingCell.layer.cornerRadius = 20
            
            if indexPath.row <= 1 {
                
                cell.lbl_Sellect_Item_Name.text = lbl_SeteingSelect_Name[indexPath.row]
                
            }

            return cell
            
        } else {
            
            let cell1 = tableView.dequeueReusableCell(withIdentifier: "SavedProfileCell") as! SavedProfileCell
            
            cell1.view_Save_Profile.layer.cornerRadius = 20
            
            cell1.lbl_Profile_Name.text = lbl_Name_Array[indexPath.row]
            
          
            return cell1
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView.tag == 0 {
            
            switch indexPath.row {
                
            case 0 : let x = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TimeVC")
                
                self.navigationController?.pushViewController(x, animated: true)
                
            case 1 : let x = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CallerVC")
                
                self.navigationController?.pushViewController(x, animated: true)
                
            case 2 : let x = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "RingtoneVC")
                
                self.navigationController?.pushViewController(x, animated: true)
                
            case 3 : let x = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "VoiceVC")
                
                self.navigationController?.pushViewController(x, animated: true)
                
            case 4 : let x = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ExtraSettingsVC")
                
                self.navigationController?.pushViewController(x, animated: true)
                
            default :
                
                break
                
            }
            
        } else {
            
            view_Alert_BG.isHidden = false
            
            let Alert = UIAlertController(title: "", message: "The phone Will be called seconds Later, don’t lock the screen and don’t return to the desktop." , preferredStyle: .alert)
            
            let but = UIAlertAction(title: "Ok", style: .default) { i in

                DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
                    
                    self.setTime()
                    
                }
                
            }
            
            Alert.addAction(but)
            
            self.present(Alert, animated: true)
        }
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 76
        
    }
    
    func setTime() {
        
        switch self.x {
            
        case 1 :
            DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
                
                let x = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CallLauncherScreenVC")
                
                self.navigationController?.pushViewController(x, animated: false)
                
            }
            
        case 2 :
            DispatchQueue.main.asyncAfter(deadline: .now() + 10) {
                
                let x = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CallLauncherScreenVC")
                
                self.navigationController?.pushViewController(x, animated: false)
                
            }
            
        case 3 :
            DispatchQueue.main.asyncAfter(deadline: .now() + 30) {
                
                let x = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CallLauncherScreenVC")
                
                self.navigationController?.pushViewController(x, animated: false)
                
            }
            
        case 4 :
            DispatchQueue.main.asyncAfter(deadline: .now() + 60) {
                
                let x = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CallLauncherScreenVC")
                
                self.navigationController?.pushViewController(x, animated: false)
                
            }
            
        case 5 :
            DispatchQueue.main.asyncAfter(deadline: .now() + 120) {
                
                let x = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CallLauncherScreenVC")
                
                self.navigationController?.pushViewController(x, animated: false)
                
            }
            
        case 6 :
            DispatchQueue.main.asyncAfter(deadline: .now() + 300) {
                
                let x = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CallLauncherScreenVC")
                
                self.navigationController?.pushViewController(x, animated: false)
                
            }
            
        case 7 :
            DispatchQueue.main.asyncAfter(deadline: .now() + 600) {
                
                let x = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CallLauncherScreenVC")
                
                self.navigationController?.pushViewController(x, animated: false)
                
            }
            
        case 8 :
            DispatchQueue.main.asyncAfter(deadline: .now() + 1200) {
                
                let x = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CallLauncherScreenVC")
                
                self.navigationController?.pushViewController(x, animated: false)
                
            }
            
        case 9 :
            DispatchQueue.main.asyncAfter(deadline: .now() + 1800) {
                
                let x = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CallLauncherScreenVC")
                
                self.navigationController?.pushViewController(x, animated: false)
                
            }
            
        case 10 :
            DispatchQueue.main.asyncAfter(deadline: .now() + 3600) {
                
                let x = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CallLauncherScreenVC")
                
                self.navigationController?.pushViewController(x, animated: false)
                
            }
            
        default:
            DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
                
                let x = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CallLauncherScreenVC")
                
                self.navigationController?.pushViewController(x, animated: false)
                
            }
            
        }
        
    }
    
}
