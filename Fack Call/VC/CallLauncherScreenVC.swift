//
//  CallLauncherScreenVC.swift
//  Fack Call
//
//  Created by Jasmin Sudani on 09/12/22.
//

import AVFoundation
import AVFAudio
import UIKit

class CallLauncherScreenVC: UIViewController {
    
    let exFunc = ExstaraFunction()
    
    let rington = RingtoneVC()

    var player: AVAudioPlayer?
    
    let exSetting = ExtraSettingsVC()
    
    let fackCall = FackCallVC()
    
    @IBOutlet weak var lbl_Caller_Name: UILabel!
    @IBOutlet weak var View_AlertHideShow: UIView!
    @IBOutlet weak var view_AlertScreen: UIView!
    @IBOutlet weak var img_Call_Launch_Bg: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
            print("is on")
        exFunc.vaibret()

        lbl_Caller_Name.text = fackCall.nameUserDefult.string(forKey: "name") ?? "Loop Bots"
           
        let urlString = Bundle.main.path(forResource: "iphone" , ofType: "mp3")
            
        do {
                
            try AVAudioSession.sharedInstance().setMode(.default)
            try AVAudioSession.sharedInstance().setActive(true, options: .notifyOthersOnDeactivation)
            
            guard let urlString = urlString else { return }
            
            player = try AVAudioPlayer(contentsOf: URL(fileURLWithPath: urlString))
            
            guard let player = player else { return }
            
            player.play()
            
        } catch {
            
            print("Something Went Wrong")
                
        }
            
        
        guard let device = AVCaptureDevice.default(for: .video) else {return}

        if device.hasFlash {

            do {

                try device.lockForConfiguration()

                if exSetting.FlasCount == true {

                    device.torchMode = .on


                } else {

                    device.torchMode = .off

                }

                device.unlockForConfiguration()

            } catch {

                print("could not be used")

            }

        } else {

            print("Torch is not available")

        }
        

//        if rington.swich_Vibreation.isOn == true {
         
            
           
//        }
        
     
    }
    
    override func viewWillAppear(_ animated: Bool) {
         
        let urlString = Bundle.main.path(forResource: rington.save_Rington_UserDefult.string(forKey: "rington") ?? "Opening" , ofType: "mp3")
        
        do {
            
            try AVAudioSession.sharedInstance().setMode(.default)
            try AVAudioSession.sharedInstance().setActive(true, options: .notifyOthersOnDeactivation)
            
            guard let urlString = urlString else { return }
            
            player = try AVAudioPlayer(contentsOf: URL(fileURLWithPath: urlString))
            
            guard let player = player else { return }
            
            player.play()
            
        } catch {
            
            print("Something Went Wrong")
            
        }
        
        guard let device = AVCaptureDevice.default(for: .video) else {return}

        if device.hasFlash {

            do {

                try device.lockForConfiguration()

                if exSetting.FlasCount == true {

                    device.torchMode = .on
                    

                } else {

                    device.torchMode = .off

                }

                device.unlockForConfiguration()

            } catch {

                print("could not be used")

            }

        } else {

            print("Torch is not available")

        }
    
    }
   
    @IBAction func but_Call_Acceot(_ sender: UIButton) {
        
        player?.stop()
        
        let x = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CallPicVC")
        
        self.navigationController?.pushViewController(x, animated: false)
        
        guard let device = AVCaptureDevice.default(for: .video) else {return}
        
        if device.hasFlash {
            
            do {
                
                try device.lockForConfiguration()
                
                device.torchMode = .off
                
                device.unlockForConfiguration()
                
            } catch {
                
                print("could not be used")
                
            }
            
        } else {
            
            print("Torch is not available")
            
        }
                
                
        
    }

    @IBAction func but_Call_Decline(_ sender: UIButton) {
        
        player?.stop()
        
        
        guard let device = AVCaptureDevice.default(for: .video) else {return}
        
        if device.hasFlash {
            
            do {
                
                try device.lockForConfiguration()
                
                device.torchMode = .off
                
                device.unlockForConfiguration()
                
            } catch {
                
                print("could not be used")
                
            }
            
        } else {
            
            print("Torch is not available")
            
        }
        
        let x = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LookScreenVC")
        self.navigationController?.pushViewController(x, animated: true)
        
    }
    

       
}
