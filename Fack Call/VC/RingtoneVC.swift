//
//  RingtoneVC.swift
//  Fack Call
//
//  Created by Jasmin Sudani on 05/12/22.
//

import UIKit

class RingtoneVC: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    
    @IBOutlet weak var swich_Rington: UISwitch!
    @IBOutlet weak var swich_Vibreation: UISwitch!
    @IBOutlet weak var view_Vibration: UIView!
    @IBOutlet weak var view_Rington: UIView!
    var arr = [Int]()
    @IBOutlet weak var but_Next: UIButton!
    @IBOutlet weak var table_Ringtone: UITableView!
    let swich_Vibreat_UserDefult = UserDefaults.standard
    let swich_Rington_UserDefult = UserDefaults.standard
    let save_Rington_UserDefult = UserDefaults.standard
    
    let lbl_Name_Array = ["Opening","Apex","Beacon","Bulletin","By The Seaside","Chimes","Circuit","Cosmic","Night Owl","Old Phone"]
    override func viewDidLoad() {
        super.viewDidLoad()
        
        table_Ringtone.dataSource = self
        table_Ringtone.delegate = self
        but_Next.layer.cornerRadius = 15
        view_Rington.layer.cornerRadius = 20
        view_Vibration.layer.cornerRadius = 20
        swich_Vibreation.isOn = swich_Vibreat_UserDefult.bool(forKey: "vibreat")
        swich_Rington.isOn = swich_Rington_UserDefult.bool(forKey: "User")
        
        print(swich_Rington.isOn)
        
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return lbl_Name_Array.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "RingtoneCell") as! RingtoneCell
        
        cell.lbl_Ringtone_Name.text = lbl_Name_Array[indexPath.row]
        
        cell.view_Ringtone.layer.cornerRadius = 20
        
        let z = arr.contains(indexPath.row)
        
        if z == true {
            
            cell.view_Ringtone.layer.borderWidth = 2
            
            cell.view_Ringtone.layer.borderColor = UIColor(hexString: "5D00CF").cgColor
            
            cell.img_Ringtine_Chek.isHidden = false
            
            
        } else {
            
            cell.view_Ringtone.layer.borderWidth = 0
            
            cell.img_Ringtine_Chek.isHidden = true
            
        }
        
        return cell
        
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let x = arr.contains(indexPath.row)
        
        if x {
            
            let k = arr.firstIndex(of: indexPath.row)!
            
            arr.remove(at: k)
            
        }
        
        else {
            
            arr.removeAll()
            
            arr.append(indexPath.row)
            
        }
        
        save_Rington_UserDefult.set(lbl_Name_Array[indexPath.row], forKey: "rington")
        
        
        tableView.reloadData()
         
    }
    
    @IBAction func but_Ringtone_Back(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
    @IBAction func but_Ringtone_Next_Action(_ sender: UIButton) {
        
        let x = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "VoiceVC")
        
        self.navigationController?.pushViewController(x, animated: true)
        
    }
    
    @IBAction func swich_Vibration_Action(_ sender: UISwitch) {
        
        if swich_Vibreation.isOn == true {
            
            swich_Vibreat_UserDefult.set(true, forKey: "vibreat")
            
        } else {
            
            swich_Vibreat_UserDefult.set(false, forKey: "vibreat")
            
        }
        
        swich_Vibreation.isOn = swich_Vibreat_UserDefult.bool(forKey: "vibreat")
        
    }
    
    @IBAction func swich_Rington_Action(_ sender: UISwitch) {
        
        if swich_Rington.isOn == true{
            
            swich_Rington_UserDefult.set(true, forKey: "User")
            
        } else {
            
            swich_Rington_UserDefult.set(false, forKey: "User")
            
        }
        
        swich_Rington.isOn = swich_Rington_UserDefult.bool(forKey: "User")
        
        
    }
}

