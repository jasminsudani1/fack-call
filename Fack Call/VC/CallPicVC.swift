//
//  CallPicVC.swift
//  Fack Call
//
//  Created by Jasmin Sudani on 09/12/22.
//

import UIKit
import AVFoundation

class CallPicVC: UIViewController {
    
    var dataBase = DataBaseHelper()
    var fackCall = FackCallVC()

    @IBOutlet weak var but_speaker: UIButton!
    @IBOutlet weak var view_Contacts: UIView!
    @IBOutlet weak var view_FaceTime: UIView!
    @IBOutlet weak var view_AddCall: UIView!
    @IBOutlet weak var view_Speaker: UIView!
    @IBOutlet weak var view_keyped: UIView!
    @IBOutlet weak var view_Mute: UIView!
    var audioPlayer = AVAudioPlayer()
    @IBOutlet weak var lbl_Caller_Name: UILabel!
    @IBOutlet weak var lbl_CallPic_Timer: UILabel!
    @IBOutlet weak var view_Cellpic_BG: UIView!
    @IBOutlet weak var img_Callpic_BG: UIImageView!
    var time : Timer = Timer()
    var count : Int = 0
    var timerCount : Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lbl_Caller_Name.text = fackCall.nameUserDefult.string(forKey: "name") ?? "Loop Bots"
        
        view_Mute.layer.cornerRadius = view_Mute.frame.height/2
        
        view_keyped.layer.cornerRadius = view_keyped.frame.height / 2
        
        view_Speaker.layer.cornerRadius = view_Speaker.frame.height / 2
        
        view_Contacts.layer.cornerRadius = view_Contacts.frame.height / 2
        
        view_AddCall.layer.cornerRadius = view_AddCall.frame.height / 2
        
        view_FaceTime.layer.cornerRadius = view_FaceTime.frame.height / 2
        
        time = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(timerCounting), userInfo: nil, repeats: true)
        
        let path = getDirectory().appendingPathComponent("\(1).m4a")
        
        do {
            
            audioPlayer = try AVAudioPlayer(contentsOf: path)
            audioPlayer.play()
            
        } catch {
            
            print(error)
            
        }
        
    }

    func createBlure(){

        let blureEffect = UIBlurEffect(style: .regular)
        let visualEffect = UIVisualEffectView(effect: blureEffect)

        visualEffect.frame = view.bounds
        visualEffect.alpha = 1


        view.addSubview(visualEffect)

    }
    
    @IBAction func but_Call_Decline_Action(_ sender: UIButton) {
        
        let x = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LookScreenVC")
        
        self.navigationController?.pushViewController(x, animated: false)
        
    }
    
    @IBAction func but_Speaker_Action(_ sender: Any) {
        
        but_speaker.isSelected.toggle()
        
        dataBase.fach()
        
        
    }
    
    @objc func timerCounting() -> Void {
        
        count += 1
        let time = hoursMinSecond(secondes: count)
        let timeString = makeTimeString( minuttes: time.0, secondes: time.1)
        lbl_CallPic_Timer.text = timeString
        
    }

    func hoursMinSecond(secondes : Int) -> (Int, Int) {
        
        return (((secondes % 3600) / 60), ((secondes % 3600) % 60))
        
    }
    
    func makeTimeString( minuttes : Int, secondes : Int) -> String {
        
        var timeString = " "
        timeString += String(format: "%02d", minuttes)
        timeString += " : "
        timeString += String(format: "%02d", secondes)
        return timeString
        
    }
 
    func getDirectory() -> URL {
        
        let path = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        
        let documentDirectory = path[0]
        
        return documentDirectory
        
    }
    
}
