//
//  VoiceVC.swift
//  Fack Call
//
//  Created by Jasmin Sudani on 10/12/22.
//

import UIKit
import AVFoundation

class VoiceVC: UIViewController, UITableViewDataSource, UITableViewDelegate, AVAudioRecorderDelegate {
    
    var arr = [Int]()
    
    var lbl_Array = ["None", "Self Recording"]
    
    @IBOutlet weak var view_Recode: UIView!
    
    @IBOutlet weak var but_Stat_And_Close: UIButton!
    
    @IBOutlet weak var but_Next: UIButton!
    
    @IBOutlet weak var table_Voic: UITableView!
    
    @IBOutlet weak var table_Voic_Recode: UITableView!
   
    @IBOutlet weak var lbl_Timer: UILabel!
    
    @IBOutlet weak var lbl_Start_And_Close: UILabel!
    
    @IBOutlet weak var but_Puse: UIButton!
    
    var recodingSession : AVAudioSession!
    var audioRecode : AVAudioRecorder!
    var audioPlayer = AVAudioPlayer()
    var numberOfcount = 0
    var time : Timer = Timer()
    var count : Int = 0
    var timerCount : Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        table_Voic.dataSource = self
        table_Voic.delegate = self
        but_Next.layer.cornerRadius = 15
        table_Voic_Recode.dataSource = self
        table_Voic_Recode.delegate = self
        
        recodingSession = AVAudioSession.sharedInstance()
        
        if let number : Int = UserDefaults.standard.object(forKey: "myNumber") as? Int {
            
            numberOfcount = number
            
        }
        
        AVAudioSession.sharedInstance().requestRecordPermission { (hasParmison) in
            
            if hasParmison {
                
                print("Accepted")
                
            }
            
        }
        
    }
   
    @IBAction func but_stat_And_Close_Action(_ sender: UIButton) {
        
        if audioRecode == nil {
            
            numberOfcount += 1
            let fileName = getDirectory().appendingPathComponent("\(numberOfcount).m4a")
            let settings = [AVFormatIDKey : Int(kAudioFormatMPEG4AAC), AVSampleRateKey : 12000, AVNumberOfChannelsKey : 1, AVEncoderAudioQualityKey : AVAudioQuality.high.rawValue]
        
            do {
                
                audioRecode = try AVAudioRecorder(url:  fileName , settings: settings)
                audioRecode.delegate = self
                audioRecode.record()
                
                but_Stat_And_Close.isSelected.toggle()
                lbl_Start_And_Close.text = "stop"
                
            } catch {
                
                displayAlert(title: "Ups!", message: "Recodeing File")
                
            }
            
        } else {
            
            audioRecode.stop()
            audioRecode = nil
            
            UserDefaults.standard.set(numberOfcount, forKey: "myNumber")
            table_Voic_Recode.reloadData()
            
            but_Stat_And_Close.isSelected.toggle()
            lbl_Start_And_Close.text = "Start"
            
        }
  
        if (timerCount) {
            
            timerCount = false
            time.invalidate()
            
        } else {
            
            timerCount = true
            time = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(timerCounting), userInfo: nil, repeats: true)
            
        }
        
        if timerCount == false {
            
            
            self.count = 0
            self.time.invalidate()
            self.lbl_Timer.text = makeTimeString(minuttes: 0, secondes: 0)
        }
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView.tag == 0 {
            
            return 2
            
        } else {
            
            return numberOfcount
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView.tag == 0 {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "VoiceCell") as! VoiceCell
            
            cell.view_Voice_Selact.layer.cornerRadius = 20
            
            cell.lbl_Voice_Type.text = lbl_Array[indexPath.row]
            let z = arr.contains(indexPath.row)
            
            
            if z == true {
                
                cell.view_Voice_Selact.layer.borderWidth = 2
                cell.view_Voice_Selact.layer.borderColor = UIColor(hexString: "5D00CF").cgColor
                cell.img_Voice_ChekBox.isHidden = false
                
            } else {
                
                cell.view_Voice_Selact.layer.borderWidth = 0
                cell.img_Voice_ChekBox.isHidden = true
                
            }
            
            return cell
            
        } else {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
            
            cell.textLabel?.text = String(indexPath.row + 1)
            cell.textLabel?.textColor = UIColor.black
        
            return cell
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if tableView.tag == 0 {
            
            let x = arr.contains(indexPath.row)
            
            if x {
                
                let k = arr.firstIndex(of: indexPath.row)!
                arr.remove(at: k)
                
            } else {
                
                arr.removeAll()
                arr.append(indexPath.row)
                
            }
            
            if indexPath.row == 1 {
                
                view_Recode.isHidden = false
                
            } else {
                
                view_Recode.isHidden = true
            }
            
        } else {
            
            let path = getDirectory().appendingPathComponent("\(indexPath.row + 1).m4a")
            
            do {
                
                audioPlayer = try AVAudioPlayer(contentsOf: path)
                audioPlayer.play()
                
            } catch {
                
                print(error)
                
            }
            
        }
     
        tableView.reloadData()
        
    }

    @IBAction func but_Voice_back(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
    @IBAction func but_Voice_Next(_ sender: UIButton) {
        
        let x = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ExtraSettingsVC")
        
        self.navigationController?.pushViewController(x, animated: true)
        
    }
    
    func getDirectory() -> URL {
        
        let path = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        
        let documentDirectory = path[0]
        
        return documentDirectory
        
    }
    
    func displayAlert(title : String, message : String) {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let but = UIAlertAction(title: "dismiss", style: .default,handler: nil)
        
        alert.addAction(but)
        
        present(alert, animated: true , completion: nil)
           
    }
    
    @objc func timerCounting() -> Void {
        
        count += 1
        let time = hoursMinSecond(secondes: count)
        let timeString = makeTimeString( minuttes: time.0, secondes: time.1)
        lbl_Timer.text = timeString
        
    }
    
    func hoursMinSecond(secondes : Int) -> (Int, Int) {
        
        return (((secondes % 3600) / 60), ((secondes % 3600) % 60))
        
    }
    
    func makeTimeString( minuttes : Int, secondes : Int) -> String {
        
        var timeString = " "
//        timeString += String(format: "%02d", hours)
//        timeString += " : "
        timeString += String(format: "%02d", minuttes)
        timeString += " : "
        timeString += String(format: "%02d", secondes)
        return timeString
        
    }
    
}

