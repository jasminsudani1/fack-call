//
//  ExtraSettingsVC.swift
//  Fack Call
//
//  Created by Jasmin Sudani on 24/12/22.
//

import UIKit

class ExtraSettingsVC: UIViewController {
    
    let dataBase = DataBaseHelper()
    
    let fackCall = FackCallVC()
    
    let time = TimeVC()
    
    var timer = Int()
    
    var name = String()
    
    var swich_Hide_Tips_On_Black_Screen_UserDefult = UserDefaults.standard

    @IBOutlet weak var view_AlertBG: UIView!
    
    @IBOutlet weak var view_Return_to_Real_Home_Screen: UIView!
    
    @IBOutlet weak var view_LED_Flash: UIView!
    
    @IBOutlet weak var view_Hide_Tips_On_Black_Screen: UIView!
    
    @IBOutlet weak var swich_Return_to_Real_Home_Screen: UISwitch!
    
    @IBOutlet weak var swich_LED_Flash: UISwitch!
    
    @IBOutlet weak var swich_Hide_Tips_On_Black_Screen: UISwitch!
    
    @IBOutlet weak var but_StartCall: UIButton!
    
    @IBOutlet weak var but_SaveProfile: UIButton!
    
    var FlasCount = true
    
    let swich_Flash_UserDefult = UserDefaults.standard
   
    override func viewDidLoad() {
        super.viewDidLoad()
        
       
        view_LED_Flash.layer.cornerRadius = 20
        view_Hide_Tips_On_Black_Screen.layer.cornerRadius = 20
        view_Return_to_Real_Home_Screen.layer.cornerRadius = 20
        but_StartCall.layer.cornerRadius = 20
        but_SaveProfile.layer.cornerRadius = 20
        swich_Hide_Tips_On_Black_Screen.isOn = swich_Hide_Tips_On_Black_Screen_UserDefult.bool(forKey: "hide")
        
        swich_LED_Flash.isOn = swich_Flash_UserDefult.bool(forKey: "myKey")
     
        print(timer)
        print(time.x)
        print(time.userDefult.integer(forKey: "myKey"))
        
        timer = time.userDefult.integer(forKey: "myKey")
        print(timer)
        
        name = fackCall.nameUserDefult.string(forKey: "name")!// ?? "Loop Bots"
        
    }
    
    @IBAction func swich_Return_to_Real_Home_Screen_Action(_ sender: UISwitch) {
      
    }
    
    @IBAction func swich_LED_Flash_Action(_ sender: UISwitch) {
        
        if swich_LED_Flash.isOn == true {
            
            swich_Flash_UserDefult.set(true, forKey: "myKey")
            
        } else {
            
            swich_Flash_UserDefult.set(false, forKey: "myKey")
            
        }
        
        swich_LED_Flash.isOn = swich_Flash_UserDefult.bool(forKey: "myKey")
        
        if swich_LED_Flash.isOn == true {
            
            FlasCount = true
            
        } else {
            
            FlasCount = false
            
        }
        
    }
    
    @IBAction func swich_Hide_Tips_On_Black_Screen_Action(_ sender: UISwitch) {
        
        if swich_Hide_Tips_On_Black_Screen.isOn == true {
            
            swich_Hide_Tips_On_Black_Screen_UserDefult.set(true , forKey: "hide")
            
        } else {
            
            swich_Hide_Tips_On_Black_Screen_UserDefult.set(false, forKey: "hide")
            
        }
        
        swich_Hide_Tips_On_Black_Screen.isOn = swich_Hide_Tips_On_Black_Screen_UserDefult.bool(forKey: "hide")
    }
    
    @IBAction func but_ExtraSettings_Back(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
    @IBAction func but_StartCall_Action(_ sender: UIButton) {
 
        dataBase.save(name: name, time: timer)
        print(timer)
        
        view_AlertBG.isHidden = false
        
        if swich_Hide_Tips_On_Black_Screen.isOn == true {
            
            self.setTime()
            
            
        } else {
            
            let Alert = UIAlertController(title: "", message: "The phone Will be called seconds Later, don’t lock the screen and don’t return to the desktop." , preferredStyle: .alert)
            
            let but = UIAlertAction(title: "Ok", style: .default) { i in
                
                self.setTime()
                
            }
            
            Alert.addAction(but)
            
            self.present(Alert, animated: true)
            
        }
        
        
    }
    
    @IBAction func but_SaveProfile_Action(_ sender: UIButton) {
        
        dataBase.save(name: name, time: timer)
        
        print(timer)
        
    }
    
    func setTime() {
        
        switch self.timer {
            
        case 1 :
            DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
                
                let x = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CallLauncherScreenVC")
                
                self.navigationController?.pushViewController(x, animated: false)
                
            }
            
        case 2 :
            DispatchQueue.main.asyncAfter(deadline: .now() + 10) {
                
                let x = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CallLauncherScreenVC")
                
                self.navigationController?.pushViewController(x, animated: false)
                
            }
            
        case 3 :
            DispatchQueue.main.asyncAfter(deadline: .now() + 30) {
                
                let x = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CallLauncherScreenVC")
                
                self.navigationController?.pushViewController(x, animated: false)
                
            }
            
        case 4 :
            DispatchQueue.main.asyncAfter(deadline: .now() + 60) {
                
                let x = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CallLauncherScreenVC")
                
                self.navigationController?.pushViewController(x, animated: false)
                
            }
            
        case 5 :
            DispatchQueue.main.asyncAfter(deadline: .now() + 120) {
                
                let x = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CallLauncherScreenVC")
                
                self.navigationController?.pushViewController(x, animated: false)
                
            }
            
        case 6 :
            DispatchQueue.main.asyncAfter(deadline: .now() + 300) {
                
                let x = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CallLauncherScreenVC")
                
                self.navigationController?.pushViewController(x, animated: false)
                
            }
            
        case 7 :
            DispatchQueue.main.asyncAfter(deadline: .now() + 600) {
                
                let x = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CallLauncherScreenVC")
                
                self.navigationController?.pushViewController(x, animated: false)
                
            }
            
        case 8 :
            DispatchQueue.main.asyncAfter(deadline: .now() + 1200) {
                
                let x = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CallLauncherScreenVC")
                
                self.navigationController?.pushViewController(x, animated: false)
                
            }
            
        case 9 :
            DispatchQueue.main.asyncAfter(deadline: .now() + 1800) {
                
                let x = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CallLauncherScreenVC")
                
                self.navigationController?.pushViewController(x, animated: false)
                
            }
            
        case 10 :
            DispatchQueue.main.asyncAfter(deadline: .now() + 3600) {
                
                let x = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CallLauncherScreenVC")
                
                self.navigationController?.pushViewController(x, animated: false)
                
            }
            
        default:
            DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
                
                let x = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CallLauncherScreenVC")
                
                self.navigationController?.pushViewController(x, animated: false)
                
            }
            
        }
        
    }
}
