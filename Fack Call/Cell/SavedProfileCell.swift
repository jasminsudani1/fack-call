//
//  SavedProfileCell.swift
//  Fack Call
//
//  Created by Jasmin Sudani on 02/12/22.
//

import UIKit

class SavedProfileCell: UITableViewCell {

    @IBOutlet weak var view_Save_Profile: UIView!
    
    @IBOutlet weak var but_Edit: UIButton!
    
    @IBOutlet weak var but_Delet: UIButton!
    
    @IBOutlet weak var lbl_Profile_Name: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
     
    }
  
}
